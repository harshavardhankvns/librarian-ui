import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isLoggedIn: {},
        apiBaseURL: "http://example.inspiroheights.com/Library.API/",
        librarianId: null,
        librarian: {},
        studentMembership: {},
        studentBooks: {},
        allEBooks: [],
        Laptops: {},
        myLaptop: {},
        allBooks: [],
        allStudents: [],
        MyNotifications: [],
    },
    getters: {
        isLoggedIn: (state) => {
            return state.isLoggedIn;
        },
        activeLibrarian: (state) => {
            return state.librarian;
        },
        studentBooks: (state) => {
            return state.studentBooks;
        },
        Laptops: (state) => {
            return state.Laptops;
        },
        Laptop: (state) => {
            return state.myLaptop;
        },
        allStudents: (state) => {
            return state.allStudents;
        },
        allEBooks: (state) => {
            return state.allEBooks;
        },
        allBooks: (state) => {
            return state.allBooks;
        },
        MyNotifications: (state) => {
            return state.MyNotifications;
        },
    },
    mutations: {
        setIsLoggedIn(state) {
            axios
                .get(state.apiBaseURL + `StudentIsLogIn.php`)
                .then((response) => {
                    state.isLoggedIn = response.data;
                    console.log(response.data);
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getLibrarian(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `/Librarians/GetById.php?Id=` +
                    state.librarianId
                )
                .then((response) => {
                    state.librarian = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        putLibrarian(state) {
            axios
                .post(state.apiBaseURL + `/Librarians/Put.php`, state.librarian)
                .then((response) => {
                    console.log(response);
                    if (response.data == "Success")
                        alert("Sucessfully updated librarian");
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getAllEBooks(state) {
            axios
                .get(state.apiBaseURL + `/EBooks/GetAll.php`)
                .then((response) => {
                    state.allEBooks = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getBooksByStudentId(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `/Books/GetByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.studentBooks = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getLaptops(state) {
            axios
                .get(state.apiBaseURL + `/Laptops/GetAll.php`)
                .then((response) => {
                    state.Laptops = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getMyLaptop(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `Laptops/GetByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.myLaptop = response.data[0];
                    console.log(response.data);
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getAllStudents(state) {
            axios
                .get(state.apiBaseURL + `/Students/GetAll.php`)
                .then((response) => {
                    state.allStudents = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getAllBooks(state) {
            axios
                .get(state.apiBaseURL + `/Books/GetAll.php`)
                .then((response) => {
                    state.allBooks = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getMyNotifications(state) {
            axios
                .get(
                    state.apiBaseURL + `/Notifications/GetByTargetId.php?TargetId=2`
                )
                .then((response) => {
                    state.MyNotifications = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
    },
    actions: {
        setIsLoggedIn(context) {
            context.commit("setIsLoggedIn");
        },
        getLibrarian(context) {
            context.commit("getLibrarian");
        },
        putLibrarian(context) {
            context.commit("putLibrarian");
        },
        getAllEBooks(context) {
            context.commit("getAllEBooks");
        },
        getBooksByStudentId(context) {
            context.commit("getBooksByStudentId");
        },
        getLaptops(context) {
            context.commit("getLaptops");
        },
        getMyLaptop(context) {
            context.commit("getMyLaptop");
        },
        getAllStudents(context) {
            context.commit("getAllStudents");
        },
        getAllBooks(context) {
            context.commit("getAllBooks");
        },
        getMyNotifications(context) {
            context.commit("getMyNotifications");
        },
    },
    modules: {},
});