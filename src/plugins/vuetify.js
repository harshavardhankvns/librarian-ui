import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);
const vuetify = new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#5E35B1',
                secondary: '5E35B1',
                accent: '#5271FF',
                error: '#b71c1c',
                info: '#5E35B1',
            },
        },
    },
})
export default vuetify;